<?php

namespace Drupal\facets_widgets\Plugin\facets\widget;

use Drupal\facets\Plugin\facets\widget\CheckboxWidget;
use Drupal\facets\FacetInterface;

/**
 * Facets state checkbox widget.
 *
 * @FacetsWidget(
 *   id = "state_checkbox",
 *   label = @Translation("List of state checkboxes"),
 *   description = @Translation("A configurable widget that shows a list of state checkboxes."),
 * )
 */
class StateCheckboxWidget extends CheckboxWidget {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $build = parent::build($facet);
    $build['#attached']['library'][] = 'facets_widgets/state_checkbox_widget';
    return $build;
  }

}
