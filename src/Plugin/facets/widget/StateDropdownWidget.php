<?php

namespace Drupal\facets_widgets\Plugin\facets\widget;

use Drupal\facets\FacetInterface;
use Drupal\facets\Plugin\facets\widget\DropdownWidget;

/**
 * Facets state dropdown widget.
 *
 * @FacetsWidget(
 *   id = "state_dropdown",
 *   label = @Translation("State dropdown"),
 *   description = @Translation("Removes the default option once there is a selection."),
 * )
 */
class StateDropdownWidget extends DropdownWidget {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    // @todo add configuration to enable the 'default option removal'.
    // @todo add same behaviour as checkboxes and radios for disabled states.
    $build = parent::build($facet);
    $build['#attached']['library'][] = 'facets_widgets/state_dropdown';
    return $build;
  }

}
