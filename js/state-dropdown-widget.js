/**
 * @file
 * Implements facets/drupal.facets.dropdown-widget
 * then removes the default option once a value has been selected.
 */

(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.facetsStateDropdownWidgetBehavior = {
    attach: function (context) {
      $('.js-facets-dropdown', context).once('facetsStateDropdownWidgetBehavior').each(function () {
        var $defaultOption = $('option:first', this);
        if (!$defaultOption.attr('selected')) {
          $defaultOption.remove();
        }
      });
    }
  };

})(jQuery, Drupal);
