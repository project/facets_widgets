/**
 * @file
 * Transforms links into a state radio list.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.facets = Drupal.facets || {};
  Drupal.behaviors.facetsRadioWidget = {
    attach: function (context, settings) {
      Drupal.facets.makeRadios(context, settings);
    }
  };

  /**
   * Converts the query string into an object.
   *
   * @param {string} queryString
   *
   * @returns {*}
   */
  Drupal.facets.parseQuery = function (queryString) {
    var query = {};
    var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (var i = 0; i < pairs.length; i++) {
      var pair = pairs[i].split('=');
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
  };

  /**
   * Removes the active facet from the query string object.
   *
   * @param {string} queryString
   *   The query string that may contain the facet.
   *
   * @param {string} facetId
   *   The facet id.
   *
   * @returns {string}
   *   The query string without the facet filter.
   */
  Drupal.facets.getDefaultQueryString = function (queryString, facetAlias) {
    var result = queryString;
    var queryStringObject = Drupal.facets.parseQuery(queryString);
    var modified = false;
    for (var i in queryStringObject) {
      if (queryStringObject.hasOwnProperty(i)) {
        var facetQueryKey = queryStringObject[i].split(':');
        if (facetQueryKey[0] === facetAlias) {
          delete queryStringObject[i];
          modified = true;
        }
      }
    }

    // If the query string has been modified construct the queryString
    // with the removed element.
    if (modified) {
      result = '?';
      var parameters = [];
      for (var j in queryStringObject) {
        if (queryStringObject.hasOwnProperty(j)) {
          parameters.push(j + '=' + queryStringObject[j]);
        }
      }
      result += parameters.join('&');
    }
    return result;
  };

  /**
   * Turns all facet links into radios.
   */
  Drupal.facets.makeRadios = function (context, settings) {
    $('.js-facets-radio-links', context).once('facets-radio-transform').each(function() {
      var $radioWidget = $(this);
      if ($radioWidget.length > 0) {

        // Add default radio ('all').
        var facetId = $radioWidget.data('drupal-facet-id');
        var facetAlias = $radioWidget.data('drupal-facet-alias');
        var defaultRadioLabel = settings.facets.radioWidget[facetId]['facet-default-radio-label'];
        // Remove the current facet id filter to get the 'all' href.
        var currentPath = window.location.pathname;
        var queryString = window.location.search;
        var allResultsQueryString = Drupal.facets.getDefaultQueryString(queryString, facetAlias);
        var allResultsHref = currentPath + allResultsQueryString;
        // Set it as the first element.
        var isDefaultActive = queryString === allResultsQueryString;
        var activeClass = isDefaultActive ? 'is-active' : '';
        var defaultRadioLink = '<li class="facet-item">' +
          '<a href="' + allResultsHref + '" class="' + activeClass + '" rel="nofollow"' +
          ' data-drupal-facet-item-id="default-value-'+ facetId +'" data-drupal-facet-item-value="0">' +
          '<span class="facet-item__value"> ' + defaultRadioLabel + '</span>' +
          '</a></li>';
        $radioWidget.prepend(defaultRadioLink);

        $radioWidget.each(function (index, widget) {
          var $widget = $(widget);
          var $widgetLinks = $widget.find('.facet-item > a');
          // Add CSS selector for the widget.
          // The Facets JS API will register handlers on that element.
          $widget.addClass('js-facets-widget');
          // Transform links to radios.
          $widgetLinks.each(Drupal.facets.makeRadio);
        });

        // We have to trigger attaching of behaviours, so that Facets JS API can
        // register handlers on radio widgets.
        Drupal.attachBehaviors(context, Drupal.settings);
      }

      // Set indeterminate value on parents having an active trail.
      $('.facet-item--expanded.facet-item--active-trail > input').prop('indeterminate', true);
    });
  };

  /**
   * Replaces a link with a radio.
   */
  Drupal.facets.makeRadio = function () {
    var $link = $(this);
    var active = $link.hasClass('is-active');
    var description = $link.html();
    var href = $link.prop('href');
    var id = $link.data('drupal-facet-item-id');
    var resultsCount = $link.data('drupal-facet-item-count');

    var radio = $('<input type="radio" class="facets-radio">')
      .prop('id', id)
      .data($link.data())
      .data('facetsredir', href);
    var label = $('<label for="' + id + '">' + description + '</label>');

    var $widget = $(this).closest('.js-facets-widget');
    var $facetItem = $link.closest('.facet-item');

    radio.on('change.facets', function (e) {
      e.preventDefault();
      Drupal.facets.disableRadioFacet($widget);
      $widget.trigger('facets_filter', [ href ]);
    });

    // Always add the disabled class for no results,
    // so we have a visual hint.
    if (resultsCount === 0) {
      $facetItem.addClass('facet-item-disabled');
    }

    if (active) {
      radio.prop('checked', true);
      label.find('.js-facet-deactivate').remove();
    }
    // If inactive and no results count, safely set as disabled.
    else if(resultsCount === 0) {
      radio.prop('disabled', true);
    }

    $link.before(radio).before(label).hide();
  };

  /**
   * Disables all facet radios in the facet and apply a 'disabled' class.
   *
   * @param {object} $facet
   *   jQuery object of the facet.
   */
  Drupal.facets.disableRadioFacet = function ($facet) {
    $facet.addClass('facets-disabled');
    $('input.facets-radio', $facet).on('click', Drupal.facets.preventRadioDefault);
    $('input.facets-radio', $facet).prop('disabled', true);
  };

  /**
   * Event listener for easy prevention of event propagation.
   *
   * @param {object} e
   *   Event.
   */
  Drupal.facets.preventRadioDefault = function (e) {
    e.preventDefault();
  };

})(jQuery, Drupal);
