/**
 * @file
 * Transforms links into state checkboxes.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.facets = Drupal.facets || {};
  Drupal.behaviors.facetsStateCheckboxWidget = {
    attach: function (context) {
      Drupal.facets.makeCheckboxes(context);
    }
  };

  /**
   * Turns all facet links into checkboxes.
   *
   * Review possible inheritance with default Facets checkboxes.
   */
  Drupal.facets.makeCheckboxes = function (context) {
    // Find all checkbox facet links and give them a checkbox.
    var $checkboxWidgets = $('.js-facets-checkbox-links', context)
      .once('facets-checkbox-transform');

    if ($checkboxWidgets.length > 0) {
      $checkboxWidgets.each(function (index, widget) {
        var $widget = $(widget);
        var $widgetLinks = $widget.find('.facet-item > a');

        // Add correct CSS selector for the widget. The Facets JS API will
        // register handlers on that element.
        $widget.addClass('js-facets-widget');

        // Transform links to checkboxes.
        $widgetLinks.each(Drupal.facets.makeCheckbox);
      });

      // We have to trigger attaching of behaviours, so that Facets JS API can
      // register handlers on checkbox widgets.
      Drupal.attachBehaviors(context, Drupal.settings);
    }

    // Set indeterminate value on parents having an active trail.
    $('.facet-item--expanded.facet-item--active-trail > input').prop('indeterminate', true);
  };

  /**
   * Replaces a link with a checked checkbox.
   */
  Drupal.facets.makeCheckbox = function () {
    var $link = $(this);
    var active = $link.hasClass('is-active');
    var description = $link.html();
    var href = $link.prop('href');
    var id = $link.data('drupal-facet-item-id');
    var resultsCount = $link.data('drupal-facet-item-count');

    var checkbox = $('<input type="checkbox" class="facets-checkbox">')
      .prop('id', id)
      .data($link.data())
      .data('facetsredir', href);
    var label = $('<label for="' + id + '">' + description + '</label>');

    var $widget = $(this).closest('.js-facets-widget');
    var $facetItem = $link.closest('.facet-item');

    //if (active && resultsCount === 0) {
    // Trigger a Views refresh, could be done so
    // the user does not have to uncheck active items
    // that does not contain results.
    //}

    checkbox.on('change.facets', function (e) {
      e.preventDefault();
      Drupal.facets.disableFacet($widget);
      $widget.trigger('facets_filter', [ href ]);
    });

    // Always add the disabled class for no results,
    // so we have a visual hint.
    if (resultsCount === 0) {
      $facetItem.addClass('facet-item-disabled');
    }

    if (active) {
      // Always let active items checked and never disable them.
      checkbox.prop('checked', true);
      label.find('.js-facet-deactivate').remove();
    }
    // If inactive and no results count, safely set as disabled.
    else if(resultsCount === 0) {
      checkbox.prop('disabled', true);
    }

    $link.before(checkbox).before(label).hide();
  };

  /**
   * Disables all facet checkboxes in the facet and apply a 'disabled' attribute.
   *
   * @param {object} $facet
   *   jQuery object of the facet.
   */
  Drupal.facets.disableFacet = function ($facet) {
    $facet.addClass('facets-disabled');
    $('input.facets-checkbox', $facet).on('click', Drupal.facets.preventDefault);
    $('input.facets-checkbox', $facet).prop('disabled', true);
  };

  /**
   * Event listener for easy prevention of event propagation.
   *
   * @param {object} e
   *   Event.
   */
  Drupal.facets.preventDefault = function (e) {
    e.preventDefault();
  };

})(jQuery, Drupal);
