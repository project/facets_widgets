# Facets Widgets

Provides additional widgets for Facets: radios and other widgets
that are optionally showing a disabled state when not results
are available for a facet item.

## Widgets

- State Radio: list of radios, supports disabled state.
- State Checkbox: list of checkboxes, supports disabled state.
- State Dropdown: dropdown that currently adds the removal of the default
option once a value has been selected, disabled state support needs 
to be implemented.
- Plain list Dropdown: does not convert html list into a select element, 
for styling purpose. To be implemented.

Disabled state can optionally be set with when the amount of results
for the facet item is 0.
 
Disabled state can optionally be set when the minimum 
amount of results for the facet item is 0.
